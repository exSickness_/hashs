#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"

static const size_t DEFAULT_SIZE = 10;

static void h_llist_destroy(struct h_llist* list);
static struct h_llist* h_llist_create(const char* key, void* value);
static size_t hashish(const char* key, size_t capacity);
static void hash_recalculate(hash* h);



hash* __hash_create(size_t capacity)
{
	hash* h = malloc(sizeof(*h));
	if(!h) {
		return(NULL);
	}

	h->data = calloc(capacity, sizeof(*h->data) );
	if(!h->data) {
		free(h);
		return(NULL);
	}

	h->capacity = capacity;
	h->size = 0;

	return(h);
}

hash* hash_create(void)
{
	return(__hash_create(DEFAULT_SIZE) );
}

void hash_destroy(hash* h)
{
	if(h) {
		for(size_t n = 0; n < h->capacity; n++) {
			h_llist_destroy(h->data[n]);
			// free(h->data[n]->value);
		}

		free(h->data);
		free(h);
	}

}
static void h_llist_obliterate(struct h_llist* list)
{
	while(list)
	{
		struct h_llist* tmp = list->next;
		free(list->value);
		free(list->key);
		free(list);

		list = tmp;
	}
}
void hash_obliterate(hash* h)
{
	if(h) {
		for(size_t n = 0; n < h->capacity; n++) {
			h_llist_obliterate(h->data[n]);
		}

		free(h->data);
		free(h);
	}

}


static size_t hashish(const char* key, size_t capacity)
{
	return( key[0] % capacity );
}

static struct h_llist* h_llist_create(const char* key, void* value)
{
	struct h_llist* node = malloc(sizeof(*node));
	if(!node) {
		return(NULL);
	}

	node->key = strdup(key);
	if(!node->key) {
		free(node);
		return(NULL);
	}
	node->value = value;
	node->next = NULL;

	return(node);
}

static void h_llist_destroy(struct h_llist* list)
{
	while(list)
	{
		struct h_llist* tmp = list->next;
		free(list->key);
		free(list);

		list = tmp;
	}
}

static void hash_recalculate(hash* h)
{
	if(!h) {
		return;
	}

	if(h->size < 0.70 * h->capacity) {
		return;
	}

	hash* hash_cpy = __hash_create(h->capacity *2);
	if(!hash_cpy) {
		return;
	}

	for(size_t n = 0; n < h->capacity; ++n)
	{
		struct h_llist* tmp = h->data[n];
		while(tmp)
		{
			hash_insert(hash_cpy, tmp->key, tmp->value);
			tmp = tmp->next;
		}
	}

	for(size_t n = 0; n < h->capacity; n++) {
		h_llist_destroy(h->data[n]);
	}
	free(h->data);

	h->capacity = hash_cpy->capacity;
	h->size = hash_cpy->size;
	h->data = hash_cpy->data;

	free(hash_cpy);
}

bool hash_insert(hash* h, const char* key, void* value)
{
	if(!h || !key) {
		return(false);
	}

	hash_recalculate(h);

	size_t idx = hashish(key, h->capacity);

	struct h_llist* tmp = h->data[idx];

	while(tmp)
	{
		if(strcmp(tmp->key, key) == 0) {
			tmp->value = value;
			return(true);
		}

		tmp = tmp->next;
	}

	struct h_llist* newHash = h_llist_create(key,value);
	if(!newHash) {
		return(false);
	}

	newHash->next = h->data[idx]; // insert to head of list
	h->data[idx] = newHash;
	h->size += 1;

	return(true);
}

void* hash_fetch(hash* h, const char* key)
{
	if(!h || !key) {
		return(NULL);
	}

	size_t idx = hashish(key, h->capacity);
	struct h_llist* tmp = h->data[idx];

	while(tmp)
	{
		if(strcmp(tmp->key, key) == 0) {
			// printf("Comparing %s & %s",key,tmp->key);
			return(tmp->value);
		}

		tmp = tmp->next;
	}
	return(NULL);
}

void hash_print(hash* h)
{
	for(size_t n = 0; n < h->capacity; ++n)
	{
		struct h_llist* tmp = h->data[n];
		while(tmp)
		{
			printf("%d : %s\n", *(int*)tmp->value,tmp->key);
			tmp = tmp->next;
		}
	}
}