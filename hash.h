#ifndef HASH_H
	#define HASH_H

#include <stdbool.h>

struct h_llist {
	char* key;
	void* value;
	struct h_llist* next;
};

typedef struct{
	size_t capacity;
	size_t size;
	struct h_llist** data;
} hash;

// typedef struct{
// 	size_t len;
// 	struct h_llist {
// 		const char* key;
// 		double value;
// 		struct h_llist* next;
// 	}** data;
// } hash;


hash* hash_create(void);
void hash_destroy(hash* h);

bool hash_insert(hash* h, const char* key, void* value);
void* hash_fetch(hash* h, const char* key);

void hash_print(hash* h);
void hash_obliterate(hash* h);





#endif