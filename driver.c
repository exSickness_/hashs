#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"


int main(void)
{
	hash* hashy = hash_create();
	int x = 75;
	hash_insert(hashy,"able",&x);

	FILE* fp = fopen("test.txt", "r+");
	if(!fp) {
		fprintf(stderr,"Error opening file.\n");
		return(1);
	}
	size_t sz = 50;
	char* buf = malloc(sz);

	while((getline(&buf,&sz,fp)) != -1)
	{
		int* num = malloc(sizeof(num));
		*num = 1;
		int* temp = 0;
		buf[strlen(buf) -1] = '\0';

		if( (temp = hash_fetch(hashy,buf)) == NULL)
		{
		}
		else {
			(*num) += (*temp);
		}
		hash_insert(hashy,buf,num);
	}


	hash_print(hashy);

	printf("cap:%zu\n",hashy->capacity);

	free(buf);
	fclose(fp);
	hash_obliterate(hashy);
	return(0);
}