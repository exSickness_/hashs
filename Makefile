CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic -fstack-usage -D _XOPEN_SOURCE=800 -D _BSD_SOURCE


driver: driver.o hash.o


.PHONY: clean debug profile

clean:
	rm driver *.o *.su

debug: CFLAGS+=-g
debug: driver

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: driver